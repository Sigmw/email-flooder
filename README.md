# E-mail flooder (PHP 8.0.13)
<h2>You can send emails to any server, however the request must come from Gmail. (Remember to enable less secure apps to be able to use: https://myaccount.google.com/lesssecureapps)</h2>
<h4>Insert the body content in contents.html</h4>

![a](https://raw.githubusercontent.com/Sigmw/email-flooder/master/htm.png)

<h4>and... php index.php</h4>

![a](https://raw.githubusercontent.com/Sigmw/email-flooder/master/flooder.png)

